
"""
	The cities in the game are contained in ordered dictionary CITIES and have properties as shown by class City

	#todo: come up with better descriptions
"""

from collections import OrderedDict
from django.utils.translation import ugettext as _
from numpy import array


class City():

	key = None  # string with max length 16
	name = '<nameless city>'
	description = 'Nothing remarkable about this city.'
	template = 'city/A.html'
	position = None  # numpy array

	def has_airfield(self, game):
		return True

	def has_train_station(self, game):
		return True

	def has_dock(self, game):
		return True

	def __unicode__(self):
		return self.name


class CityA(City):
	# vehicle building city
	key = 'dongdji'
	name = _('Dongji')  # (sounds like) mandarin: engine
	description = _('City of engines')
	template = 'city/A.html'
	position = array([30, 10])


class CityB(City):
	# smugglers
	key = 'umbra'
	name = _('Umbra')  # quanya: shadow
	description = _('Shady place amongst trees')
	template = 'city/B.html'
	position = array([70, 10])


class CityC(City):
	# mining
	key = 'eren'
	name = _('Eren')  # quanya: steel
	description = _('Miners to the core')
	template = 'city/C.html'
	position = array([80, 30])


class CityD(City):
	# university
	key = 'athenium'
	name = _('Athenium')  # loosely based on latin: university
	description = _('')
	template = 'city/D.html'
	position = array([85, 60])


class CityE(City):
	# financial center
	key = 'mirwa'
	name = _('Mirwa')  # quanya: valuable
	description = _('')
	template = 'city/E.html'
	position = array([65, 10])


class CityF(City):
	# military (incl. navy)
	key = 'bandow'
	name = _('Bandow')  # (sounds like) mandarin: peninsula
	description = _('')
	template = 'city/F.html'
	position = array([55, 70])


class CityG(City):
	# capital, market
	key = 'midden'
	name = _('Midden')  # dutch: center
	description = _('')
	template = 'city/G.html'
	position = array([50, 40])


class CityH(City):
	# agriculture, recreation
	key = 'millet'
	name = _('Millet')
	description = _('')
	template = 'city/H.html'
	position = array([25, 70])


class CityI(City):
	# is on a mountain, religion
	key = 'skybreach'
	name = _('Skybreach')
	description = _('')
	template = 'city/I.html'
	position = array([15, 35])


CITY_LIST = [
	CityA,
	CityB,
	CityC,
	CityD,
	CityE,
	CityF,
	CityG,
	CityH,
	CityI,
]


CITIES = OrderedDict()
for city in CITY_LIST:
	CITIES[city.key] = city


