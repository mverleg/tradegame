
"""
	Calculate all distances for each type of transport
"""

from numpy.linalg import norm
from .cities import City, CITIES
from vehicle.models.vehicle import Vehicle


def dist(city1, city2, vehicle):
	"""
		@param city1: either a City or a key identifying one
		@param: vehicle: either a Vehicle or a string with a vehicle type (in Vehicle.VEHICLE_TYPE)
	"""
	if not _issubclass_flexible(city1, City):
		""" if this throws KeyError, the city does not exist and your code has a problem """
		city1 = CITIES[city1]
	else:
		assert city1 in list(CITIES.values())
	if not _issubclass_flexible(city2, City):
		city2 = CITIES[city2]
	else:
		assert city1 in list(CITIES.values())
	if _issubclass_flexible(vehicle, Vehicle):
		transport_type = vehicle.type
	else:
		assert vehicle in [vt[0] for vt in Vehicle.VEHICLE_TYPE]
		transport_type = vehicle
	return _dist(city1, city2, transport_type)


def _issubclass_flexible(C, B):
	try:
		return issubclass(C, B)
	except TypeError:
		return False


def _dist(city1, city2, transport_type):
	if transport_type == Vehicle.CARAVAN:
		return int(round(norm(city1.position - city2.position)))  # todo
	elif transport_type == Vehicle.TRAIN:
		return int(round(norm(city1.position - city2.position)))  # todo
	elif transport_type == Vehicle.BOAT:
		return int(round(norm(city1.position - city2.position)))  # todo
	elif transport_type == Vehicle.AIRSHIP:
		return int(round(norm(city1.position - city2.position)))  # todo
	raise Exception('no distances defined for vehicle type "%s"' % transport_type)


