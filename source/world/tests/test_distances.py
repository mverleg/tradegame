
from vehicle.models.vehicle import Vehicle
from world.cities import CITIES
from world.distances import dist


def test_flight_distances():
	"""
		Test that flight distances are the shortest ones.
	"""
	for city1 in CITIES:
		for city2 in CITIES:
			print(city1, city2, dist(city1, city2, Vehicle.AIRSHIP))


def triangle_inequality():
	"""
		No need to check that the direct way is always shortest for flight, because distances are based on positions for flight. It doesn't necessarily have to hold for non-direct roads (other ways of transport).
	"""
	pass


