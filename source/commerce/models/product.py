
from django.db import models
from misc.fields.integer import BigPositiveIntegerField
from commerce.products import PRODUCTS


class ProductBatch(models.Model):

	kind = models.CharField(choices = PRODUCTS)
	amount = models.PositiveIntegerField(default = 0, help_text = 'How many units this batch contains')
	value = BigPositiveIntegerField(default = 0, help_text = 'The value paid or asked for one unit')

	class Meta:
		abstract = True
		app_label = 'commerce'


