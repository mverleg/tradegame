
from django.core.validators import MaxValueValidator
from django.db import models
from misc.classes.auto_subclass import AutoSubclassManager
from misc.fields.integer import BigPositiveIntegerField
from game.models.game import Game
from settings import AUTH_USER_MODEL


class Player(models.Model):

	name = models.CharField(max_length = 32)
	game = models.ForeignKey(Game, related_name = 'players')

	cash = BigPositiveIntegerField(default = 0, help_text = 'money on your person')
	savings = BigPositiveIntegerField(default = 0, help_text = 'money in bank savings')
	loan = BigPositiveIntegerField(default = 0, help_text = 'money owed to the bank')
	mortgage = BigPositiveIntegerField(default = 0, help_text = 'money owed ')

	interest_help = 'interest rate recieved or paid over current balance, per year in per mille'
	savings_interest = models.PositiveIntegerField(default = 0, help_text = interest_help)
	loan_interest = models.PositiveIntegerField(default = 5, help_text = interest_help)
	mortgage_interest = models.PositiveIntegerField(default = 3, help_text = interest_help)
	loan_limit = BigPositiveIntegerField(default = 100000)
	mortgage_limit = BigPositiveIntegerField(default = 500000)

	class Meta:
		app_label = 'player'

	objects = AutoSubclassManager()


class HumanPlayer(Player):

	user = models.ForeignKey(AUTH_USER_MODEL, related_name = 'players')

	class Meta:
		app_label = 'player'


class AIPlayer(Player):

	""" playing style """
	aggression = models.PositiveSmallIntegerField(default = 50, validators = [MaxValueValidator(100)])
	risk = models.PositiveSmallIntegerField(default = 50, validators = [MaxValueValidator(100)])

	class Meta:
		app_label = 'player'


#todo: player-made AIs


