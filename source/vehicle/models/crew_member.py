
from django.db import models
from misc.fields.integer import BigPositiveIntegerField
from vehicle.models.vehicle import Vehicle


class CrewMember(models.Model):

	SKILL_LEVELS = (
		(1, 'terrible'),
		(2, 'bad'),
		(3, 'okay'),
		(4, 'good'),
		(5, 'excellent'),
	)

	vehicle = models.ForeignKey(Vehicle, related_name = 'crew')
	industrious = models.SmallIntegerField(choices = SKILL_LEVELS, help_text = 'Manual labour diligence and strength')
	combat = models.SmallIntegerField(choices = SKILL_LEVELS, help_text = 'Ability in combat with attackers')
	navigation = models.SmallIntegerField(choices = SKILL_LEVELS, help_text = 'Ability to arrive places without getting lost')
	service = models.SmallIntegerField(choices = SKILL_LEVELS, help_text = 'Service towards passengers')
	maintenance = models.SmallIntegerField(choices = SKILL_LEVELS, help_text = 'Ability to repair problems with vehicles')
	marketing = models.SmallIntegerField(choices = SKILL_LEVELS, help_text = 'Ability to promote the company to customers')
	salary = BigPositiveIntegerField(help_text = 'Amount of money recieved per month')

	class Meta:
		app_label = 'vehicle'


