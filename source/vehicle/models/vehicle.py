
from django.db import models
from misc.fields.integer import StrictlyPositiveIntegerField, BigPositiveIntegerField


class Vehicle(models.Model):

	CARAVAN, TRAIN, BOAT, AIRSHIP = 'caravan', 'train', 'ship', 'airship'
	VEHICLE_TYPE = (
		(CARAVAN,  'caravan'),
		(TRAIN,    'train'),
		(BOAT,     'boat'),
		(AIRSHIP,  'airship'),
	)
	type = models.CharField(max_length = 8, choices = VEHICLE_TYPE)
	fuel = models.PositiveIntegerField(default = 0, help_text = 'Current fuel supply')
	velocity = models.PositiveIntegerField(default = 0, help_text = '')

	cargo_mass = StrictlyPositiveIntegerField(default = 0, help_text = 'How many units of mass can this vehicle transport?')
	cargo_volume = StrictlyPositiveIntegerField(default = 0, help_text = 'How much space does this vehicle have?')
	fuel_consumption = models.PositiveIntegerField(default = 0, help_text = 'How many units of fuel are consumed per unit of length?')
	fuel_limit = StrictlyPositiveIntegerField(default = 0, help_text = 'How many units of fuel can this vehicle carry?')
	#new_value = StrictlyPositiveIntegerField(default = 0, help_text = 'New value of this vehicle')

	#engines (or horses)

	class Meta:
		app_label = 'vehicle'

