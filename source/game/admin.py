
from django.contrib import admin
from player.models.player import HumanPlayer, AIPlayer


admin.site.register(HumanPlayer)
admin.site.register(AIPlayer)


