
from datetime import timedelta
from django.db import models
from timedelta import TimedeltaField


class Game(models.Model):

	CREATED, JOINING, PLAYING, COMPLETED = 'create', 'join', 'play', 'complete'
	STATES = (
		(CREATED,   'Being created'),
		(JOINING,   'Waiting for people to join'),
		(PLAYING,   'Currently being played'),
		(COMPLETED, 'The game is over')
	)

	name = models.CharField(max_length = 32)
	max_players = models.PositiveSmallIntegerField(default = 4)
	state = models.CharField(max_length = 8, choices = STATES, default = CREATED)
	active_player = models.ForeignKey('player.Player', related_name = 'active_turn_games')
	turn_time = TimedeltaField(default = timedelta(seconds = 86400), help_text = 'Time available for a turn (real world)')
	turn_start = models.DateTimeField(blank = True, null = True)
	created = models.DateTimeField(null = True, auto_now = True)

	class Meta:
		app_label = 'game'


