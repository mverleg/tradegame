
from django import forms
from game.models.game import Game


class StartGame(forms.ModelForm):
	class Meta:
		model = Game
		fields = ['name', 'turn_time',]


