
from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
from game.views.list_games import list_games
from game.views.start_game import start_game


urlpatterns = patterns('',
    url(r'^$', lambda request: redirect(to = reverse('list_games')), name = 'game'),
    url(r'^start/$', start_game, name = 'start_game'),
    url(r'^list/$', list_games, name = 'list_games'),
)


