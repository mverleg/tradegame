
from django.shortcuts import render
from game.models.game import Game


def list_games(request):
	games = Game.objects.filter(state = Game.JOINING)
	return render(request, 'list_games.html', {
		'games': games,
	})


