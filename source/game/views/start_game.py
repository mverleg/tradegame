
from django.shortcuts import render
from game.forms import StartGame


def start_game(request):
	form = StartGame(data = request.POST or None)
	return render(request, 'start_game.html', {
		'form': form,
	})


