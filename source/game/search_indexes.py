
from django.utils import timezone
from haystack import indexes
from game.models.game import Game


class GameIndex(indexes.SearchIndex, indexes.Indexable):

	text = indexes.CharField(document = True, use_template = True, template_name = 'index/game.txt')
	name = indexes.CharField(model_attr = 'name')

	def get_model(self):
		return Game

	def index_queryset(self, using = None):
		return self.get_model().objects.filter(created__lte = timezone.now(), started__gte = timezone.now())


